package com.nqt.lovecount.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "holiday_table")
public class Holiday {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String tittle;
    private int day;
    private String month;
    private int prioryti;


    public Holiday() {
    }
    @Ignore
    public Holiday(String tittle, int day, String month) {
        this.tittle = tittle;
        this.day = day;
        this.month = month;
    }
    @Ignore
    public Holiday(int id, String tittle, int day, String month) {
        this.id = id;
        this.tittle = tittle;
        this.day = day;
        this.month = month;
    }

    public Holiday(int id, String tittle, int day, String month, int prioryti) {
        this.id = id;
        this.tittle = tittle;
        this.day = day;
        this.month = month;
        this.prioryti = prioryti;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrioryti() {
        return prioryti;
    }

    public void setPrioryti(int prioryti) {
        this.prioryti = prioryti;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
