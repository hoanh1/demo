package com.nqt.lovecount.database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.nqt.lovecount.model.Holiday;

import java.util.List;

public class HolidayRespository {
    private HolidayDAO holidayDAO;
    private LiveData<List<Holiday>> allHolidays;

    public HolidayRespository(Application application){
        HolidayDatabase database = HolidayDatabase.getInstance(application);
        holidayDAO = database.holidayDAO();
        allHolidays = holidayDAO.getAllHolidayLivedata();
    }

    public void insert(Holiday holiday){
        new insertHolidayAsyntask(holidayDAO).execute(holiday);
    }
    private static class insertHolidayAsyntask extends AsyncTask<Holiday,Void,Void>{
        private HolidayDAO holidayDAO;
        public insertHolidayAsyntask(HolidayDAO holidayDAO){
            this.holidayDAO = holidayDAO;
        }
        @Override
        protected Void doInBackground(Holiday... holidays) {
            holidayDAO.insert(holidays[0]);
            return null;
        }
    }

    public void update(Holiday holiday){
        new updateHolidayAsyntask(holidayDAO).execute(holiday);
    }
    private static class updateHolidayAsyntask extends AsyncTask<Holiday,Void,Void>{
        private HolidayDAO holidayDAO;
        public updateHolidayAsyntask(HolidayDAO holidayDAO){
            this.holidayDAO = holidayDAO;
        }
        @Override
        protected Void doInBackground(Holiday... holidays) {
            holidayDAO.update(holidays[0]);
            return null;
        }
    }
    public void delete(Holiday holiday){
        new insertHolidayAsyntask(holidayDAO).execute(holiday);
    }
    private static class deleteHolidayAsyntask extends AsyncTask<Holiday,Void,Void>{
        private HolidayDAO holidayDAO;
        public deleteHolidayAsyntask(HolidayDAO holidayDAO){
            this.holidayDAO = holidayDAO;
        }
        @Override
        protected Void doInBackground(Holiday... holidays) {
            holidayDAO.delete(holidays[0]);
            return null;
        }
    }

}
