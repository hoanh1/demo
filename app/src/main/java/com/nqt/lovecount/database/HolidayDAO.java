package com.nqt.lovecount.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.nqt.lovecount.model.Holiday;

import java.util.List;

@Dao
public interface HolidayDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Holiday holiday);

    @Update
    void update(Holiday holiday);

    @Delete
    void delete(Holiday holiday);

    @Query("select * from holiday_table")
    List<Holiday> getAllHoliDay();

    @Query("select * from holiday_table")
    LiveData<List<Holiday>> getAllHolidayLivedata();

}
