package com.nqt.lovecount.utils;

public class Const {
    public static final String KEY_TITLE = "title";
    public static final String KEY_DAY = "day";
    public static final String KEY_MONTH = "month";
    public static final String KEY_PRIORITY = "priority";
    public static final int CODE_ADD_HOLIDAY = 1;
}
